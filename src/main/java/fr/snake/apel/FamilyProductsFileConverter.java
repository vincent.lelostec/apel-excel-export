package fr.snake.apel;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.font.Standard14Fonts.FontName;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.util.IOUtils;
import org.vandeseer.easytable.RepeatedHeaderTableDrawer;
import org.vandeseer.easytable.settings.BorderStyle;
import org.vandeseer.easytable.settings.HorizontalAlignment;
import org.vandeseer.easytable.settings.VerticalAlignment;
import org.vandeseer.easytable.structure.Row;
import org.vandeseer.easytable.structure.Row.RowBuilder;
import org.vandeseer.easytable.structure.Table;
import org.vandeseer.easytable.structure.Table.TableBuilder;
import org.vandeseer.easytable.structure.cell.AbstractCell;
import org.vandeseer.easytable.structure.cell.ImageCell;
import org.vandeseer.easytable.structure.cell.TextCell;

/**
 * Converts Excel data to a structured and formatted PDF document, presenting family-related information along with
 * associated product details in a tabular layout. The ExcelToPDF class serves as the main entry point for the
 * conversion process, allowing users to specify the full path of the Excel file and the target folder path for the
 * resulting PDF. The conversion involves extracting data from the Excel file, organizing it by families, and generating
 * a PDF document with a clear title section and a detailed table for each family.
 */
public class FamilyProductsFileConverter extends FileConverter {
    /** Logger */
    protected static final Logger LOGGER = LogManager.getLogger(FamilyProductsFileConverter.class);
    /** Font size for pdf titles */
    private static final int TITLE_FONT_SIZE = 20;
    /** Bold font */
    private static final PDType1Font BOLD_FONT = new PDType1Font(FontName.HELVETICA_BOLD);
    /** Table header color */
    private static final Color HEADER_COLOR = Color.DARK_GRAY;
    /** Color for table even row (blue) */
    private static final Color EVEN_ROW_BG_COLOR = new Color(230, 230, 230);
    /** Color for table odd row (light blue) */
    private static final Color ODD_ROW_BG_COLOR = new Color(205, 205, 205);
    /** Table headers */
    private static final List<String> HEADERS = Arrays.asList("", "NB", "PRODUIT", "COMMENTAIRES");
    /** Checkbox image */
    private PDImageXObject checkboxImage;

    public FamilyProductsFileConverter(File excelFile, File targetDir) {
        super(excelFile, targetDir);
    }

    @Override
    void convert() throws IOException {
        Map<String, Family> families = new TreeMap<>();

        LOGGER.info("Start parsing file {}", inputFile.getAbsolutePath());

        try (var workbook = WorkbookFactory.create(inputFile)) {
            Sheet sheet = workbook.getSheetAt(0);

            // excel header row, look for column indexes
            LOGGER.info("Parsing the header..");

            org.apache.poi.ss.usermodel.Row headerRow = sheet.getRow(0);
            int refCommandIndex = -1;
            int nomPayeurIndex = -1;
            int prenomPayeurIndex = -1;
            int tarifIndex = -1;
            int montantIndex = -1;
            for (int i = 0; i <= headerRow.getLastCellNum(); i++) {
                if (headerRow.getCell(i) != null) {
                    switch (headerRow.getCell(i).getStringCellValue()) {
                        case "Référence commande" -> refCommandIndex = i;
                        case "Nom payeur" -> nomPayeurIndex = i;
                        case "Prénom payeur" -> prenomPayeurIndex = i;
                        case "Tarif" -> tarifIndex = i;
                        case "Montant tarif" -> montantIndex = i;
                        default -> {
                            // nothing to do
                        }
                    }
                }
            }

            if (refCommandIndex <= -1 || nomPayeurIndex <= -1 || tarifIndex <= -1) {
                throw new IllegalArgumentException(
                    "One of mandatory columns (Référence commande, Nom payeur, Prénom payeur, Tarif) is absent.");
            }

            LOGGER.info("Parsing product lines..");
            for (var i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
                org.apache.poi.ss.usermodel.Row row = sheet.getRow(i);
                Cell commandCell = row.getCell(refCommandIndex);

                // retrieve info
                String commandRef = NumberToTextConverter.toText(commandCell.getNumericCellValue());
                String name = sanitize(row.getCell(nomPayeurIndex).getStringCellValue().toUpperCase());

                String firstName = sanitize(row.getCell(prenomPayeurIndex).getStringCellValue());
                firstName = Pattern
                    .compile("^.")
                    .matcher(firstName)
                    .replaceFirst(m -> m.group().toUpperCase());
                String familyName = name + " " + firstName;
                String productName = sanitize(row.getCell(tarifIndex).getStringCellValue());
                double amount = row.getCell(montantIndex).getNumericCellValue();

                // get or create the family
                Family family = families.computeIfAbsent(familyName,
                    k -> new Family(familyName, commandRef, new TreeMap<>()));

                // add the product
                family.addProduct(productName, amount);
            }
        }

        createPDF(getOutpuFilePath(".pdf"), families);
    }

    /**
     * Creates a PDF document based on the provided Excel data.
     * @param pdfPath The full path where the generated PDF file will be saved.
     * @param families A Map containing family information and product details.
     * @throws IOException If an I/O error occurs during PDF creation.
     */
    private void createPDF(Path pdfPath, Map<String, Family> families) throws IOException {
        LOGGER.info("Creating the PDF file...");
        try (PDDocument document = new PDDocument(); OutputStream pdfOS = Files.newOutputStream(pdfPath)) {

            final byte[] sampleBytes = IOUtils.toByteArray(
                Objects.requireNonNull(FileConversion.class.getClassLoader().getResourceAsStream("check.png")));
            checkboxImage = PDImageXObject.createFromByteArray(document, sampleBytes, "check");

            for (var family : families.entrySet()) {
                Family familyValue = family.getValue();
                PDPage page = new PDPage(PDRectangle.A4);
                document.addPage(page);

                PDPageContentStream contentStream = new PDPageContentStream(document, page);

                printTitle(page, contentStream, familyValue, familyValue.products.values());
                printTable(document, page, contentStream, familyValue.products);

                contentStream.close();

                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(familyValue);
                }
            }
            document.save(pdfOS);
        }
        LOGGER.info("PDF created in {}", pdfPath);
    }

    /**
     * Prints the title section of the PDF document.
     * @param page The PDPage object representing a page in the PDF.
     * @param contentStream The PDPageContentStream for adding content to the page.
     * @param family The Family object containing family details.
     * @param values Collection of values used in the title section.
     * @throws IOException If an I/O error occurs during text rendering.
     */
    private void printTitle(PDPage page, PDPageContentStream contentStream, Family family,
        Collection<Integer> values) throws IOException {
        int nbProducts = values.stream()
            .mapToInt(Integer::intValue)
            .sum();

        contentStream.beginText();
        contentStream.setFont(BOLD_FONT, TITLE_FONT_SIZE);
        contentStream.newLineAtOffset(20, page.getMediaBox().getUpperRightY() - 25);
        contentStream.showText(family.name);
        contentStream.endText();

        contentStream.beginText();
        contentStream.newLineAtOffset(20, page.getMediaBox().getUpperRightY() - 35);
        contentStream.setFont(BOLD_FONT, 9);
        contentStream.showText("Référence commande : " + family.refCommand);
        contentStream.endText();

        contentStream.beginText();
        contentStream.newLineAtOffset(20, page.getMediaBox().getUpperRightY() - 45);
        contentStream.setFont(BOLD_FONT, 9);
        contentStream.showText("Nombre de produits : " + nbProducts);
        contentStream.endText();
    }

    /**
     * Prints the table section of the PDF document.
     * @param document The PDDocument object representing the entire PDF document.
     * @param page The PDPage object representing a page in the PDF.
     * @param contentStream The PDPageContentStream for adding content to the page.
     * @param products Map containing product details for a family.
     * @throws IOException If an I/O error occurs during table creation.
     */
    private void printTable(PDDocument document, PDPage page, PDPageContentStream contentStream,
        Map<String, Integer> products)
        throws IOException {
        TableBuilder tableBuilder = Table.builder()
            .addColumnsOfWidth(20, 30, 360, 150)
            .padding(2)
            .borderColor(Color.WHITE);

        buildHeader(tableBuilder);
        buildData(products, tableBuilder);

        RepeatedHeaderTableDrawer tableDrawer = RepeatedHeaderTableDrawer.builder()
            .contentStream(contentStream)
            .table(tableBuilder.build())
            .startX(20f)
            .startY(page.getMediaBox().getUpperRightY() - 50f)
            .endY(20f)
            .build();

        tableDrawer.draw(() -> document, () -> new PDPage(PDRectangle.A4), 50f);
    }

    /**
     * Builds the header row of the table in the PDF document.
     * @param tableBuilder The TableBuilder used to construct the table structure.
     */
    private void buildHeader(TableBuilder tableBuilder) {
        RowBuilder headerBuilder = Row
            .builder()
            .horizontalAlignment(HorizontalAlignment.CENTER)
            .verticalAlignment(VerticalAlignment.MIDDLE)
            .borderStyle(BorderStyle.SOLID)
            .borderWidth(1)
            .backgroundColor(HEADER_COLOR)
            .textColor(Color.WHITE)
            .font(BOLD_FONT)
            .fontSize(9)
            .height(25f);

        HEADERS.forEach(header -> headerBuilder
            .add(TextCell
                .builder()
                .text(header)
                .build()
            )
        );

        tableBuilder.addRow(headerBuilder.build());
    }

    /**
     * Builds the data rows of the table in the PDF document.
     * @param products Map containing product details for a family.
     * @param tableBuilder The TableBuilder used to construct the table structure.
     */
    private void buildData(Map<String, Integer> products, TableBuilder tableBuilder) {
        boolean toogle = false;
        for (var product : products.entrySet()) {
            RowBuilder dataBuilder = Row
                .builder()
                .borderWidth(1)
                .font(BOLD_FONT)
                .padding(5)
                .textColor(Color.BLACK)
                .backgroundColor(toogle ? EVEN_ROW_BG_COLOR : ODD_ROW_BG_COLOR)
                .height(20f)
                .add(createCheckCell())
                .add(TextCell
                    .builder()
                    .text(String.valueOf(product.getValue()))
                    .fontSize(12)
                    .horizontalAlignment(HorizontalAlignment.CENTER)
                    .build()
                )
                .add(TextCell
                    .builder()
                    .text(String.valueOf(product.getKey()))
                    .fontSize(9)
                    .horizontalAlignment(HorizontalAlignment.LEFT)
                    .build()
                )
                .add(TextCell
                    .builder()
                    .fontSize(9)
                    .text("")
                    .build()
                );
            tableBuilder.addRow(dataBuilder.build());
            toogle = !toogle;
        }
    }

    /**
     * Creates an AbstractCell representing a checkbox cell in the table.
     * @return The AbstractCell containing an image of a checkbox.
     */
    private AbstractCell createCheckCell() {
        return ImageCell.builder().borderWidth(1).image(checkboxImage).maxHeight(12).build();
    }

    /**
     * Represents a family with a name, reference command, and a map of products.
     */
    static class Family {
        /** Amount format */
        private static final DecimalFormat DF = new DecimalFormat("#0.00€");
        /** The family name */
        private final String name;
        /** The family command reference */
        private final String refCommand;
        /** The family list of products */
        private final Map<String, Integer> products;
        /** The family command total amount */
        double totalAmount = 0f;

        /**
         * Builds a new Family.
         * @param name the family name
         * @param refCommand the family command reference
         * @param products the list of products
         */
        public Family(String name, String refCommand, Map<String, Integer> products) {
            this.name = name;
            this.refCommand = refCommand;
            this.products = products;
        }

        /**
         * Add a product to the family list of products.
         * @param productName the product name
         * @param amount the product amount
         */
        void addProduct(String productName, double amount) {
            totalAmount += amount;
            if (products.containsKey(productName)) {
                products.merge(productName, 1, Integer::sum);
            } else {
                products.put(productName, 1);
            }
        }

        @Override
        public String toString() {
            return name + " => " +
                "refCommand=" + refCommand +
                "; nb products=" + products.values().stream().mapToInt(Integer::intValue).sum() +
                "; totalAmount=" + DF.format(totalAmount);
        }
    }
}
