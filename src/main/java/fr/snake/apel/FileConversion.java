package fr.snake.apel;

import java.io.File;
import java.io.IOException;

/**
 * Main class to perform file conversion.
 */
public class FileConversion {

    /**
     * Main entry point for the FileConversion application.
     * @param args Command-line arguments. Should contain the full path of the input file, the target folder path for
     * the converted file and the export type.
     * @throws IOException If an I/O error occurs during the conversion process.
     */
    public static void main(String[] args) throws IOException {
        if (args == null || args.length <= 2) {
            throw new IllegalArgumentException(
                "Usage: java -jar ... fr.snake.apel.FileConversion full_input_file_path full_output_dir_path convert_type[TSHIRT|PRODUITS]");
        }

        // control args
        String inputFileArg = args[0];
        File inputFile = new File(inputFileArg);

        if (!inputFile.exists() && !inputFile.isFile()) {
            throw new IllegalArgumentException(inputFileArg + " doesn't exists or is not a valid file.");
        }

        String outputDirArg = args[1];
        File outputDir = new File(outputDirArg);
        if (!outputDir.exists() || !outputDir.isDirectory()) {
            throw new IllegalArgumentException(outputDirArg + " doesn't exists or is not a valid directory.");
        }

        String convertTypeArg = args[2];
        FileConverter fileConverter = FileConverter.getConverter(convertTypeArg, inputFile, outputDir);
        fileConverter.convert();
    }
}