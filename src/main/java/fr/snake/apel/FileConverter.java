package fr.snake.apel;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.commons.lang3.StringUtils;

/**
 * A converter to convert a file.
 */
public abstract class FileConverter {

    /** The input file to convert */
    final File inputFile;
    /** The output directory where to put the converted file */
    final File outputDirectory;

    /**
     * Creates the converter.
     * @param inputFile the input file to convert
     * @param outputDirectory the output directory where to put the converted file
     */
    FileConverter(File inputFile, File outputDirectory) {
        this.inputFile = inputFile;
        this.outputDirectory = outputDirectory;
    }

    /**
     * Gets the converter depending on conversion type.
     * @param convertType the conversion type
     * @param inputFile the input file to convert
     * @param outputDirectory the output directory where to put the converted file
     * @return the converter associated to the given convert type
     */
    public static FileConverter getConverter(String convertType, File inputFile, File outputDirectory) {
        ConvertType type = ConvertType.valueOf(convertType);
        switch (type) {
            case TSHIRT -> {
                return new TShirtsFileConverter(inputFile, outputDirectory);
            }
            case PRODUITS -> {
                return new FamilyProductsFileConverter(inputFile, outputDirectory);
            }
            default -> throw new IllegalArgumentException(
                "Unknown type " + convertType + ". Type can takes values TSHIRT or PRODUITS");
        }
    }

    /**
     * Performs the file conversion.
     * @throws IOException if error occurs when converting
     */
    abstract void convert() throws IOException;

  /**
   * Sanitizes the input string by removing any characters that are not letters, digits, single
   * quotes, or whitespace characters.
   * @param input the input string to be sanitized
   * @return the sanitized string with only valid characters
   */
    public static String sanitize(String input) {
        if (input == null) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (isValidChar(c)) {
                sb.append(c);
            }
        }
        return sb.toString().trim();
    }

  /**
   * Checks if the given character is a valid character for the string sanitization. Valid
   * characters are letters, digits, single quotes, and whitespace characters.
   * @param c the character to be checked
   * @return true if the character is valid, false otherwise
   */
    private static boolean isValidChar(char c) {
        return Character.isLetterOrDigit(c) || c == '\'' || Character.isWhitespace(c);
    }


    /**
     * Gets output fil path which is the concatenation of output dir, input file name without extension and given
     * extension.
     * @param extension the output file extension
     */
    Path getOutpuFilePath(String extension) {
        if (StringUtils.isBlank(extension) || !extension.startsWith(".")) {
            throw new IllegalArgumentException("Invalid extension " + extension);
        }
        String fileName = inputFile.getName();

        String pdfName = ((fileName.lastIndexOf('.') > 0)
            ? fileName.substring(0, fileName.lastIndexOf('.'))
            : fileName) + extension;

        return Paths.get(outputDirectory.getAbsolutePath(), pdfName);
    }

    /**
     * Available conversion types.
     */
    enum ConvertType {
        TSHIRT, PRODUITS
    }

}
