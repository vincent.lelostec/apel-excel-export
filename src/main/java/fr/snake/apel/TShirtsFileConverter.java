package fr.snake.apel;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.font.Standard14Fonts.FontName;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.vandeseer.easytable.RepeatedHeaderTableDrawer;
import org.vandeseer.easytable.settings.BorderStyle;
import org.vandeseer.easytable.settings.HorizontalAlignment;
import org.vandeseer.easytable.settings.VerticalAlignment;
import org.vandeseer.easytable.structure.Row;
import org.vandeseer.easytable.structure.Row.RowBuilder;
import org.vandeseer.easytable.structure.Table;
import org.vandeseer.easytable.structure.Table.TableBuilder;
import org.vandeseer.easytable.structure.cell.TextCell;

/**
 * Converter to convert Excel T-Shirts file from APEL export to PDF resuming t-shirts command.
 */
public class TShirtsFileConverter extends FileConverter {
    /** Logger */
    protected static final Logger LOGGER = LogManager.getLogger(TShirtsFileConverter.class);
    /** Font size for pdf titles */
    private static final int TITLE_FONT_SIZE = 20;
    /** Bold font */
    private static final PDType1Font BOLD_FONT = new PDType1Font(FontName.HELVETICA_BOLD);
    /** Table header color */
    private static final Color HEADER_COLOR = Color.DARK_GRAY;
    /** Color for table even row (blue) */
    private static final Color EVEN_ROW_BG_COLOR = new Color(230, 230, 230);
    /** Color for table odd row (light blue) */
    private static final Color ODD_ROW_BG_COLOR = new Color(205, 205, 205);
    /** Table headers */
    private static final List<String> HEADERS = Arrays.asList("TAILLE", "COULEUR", "NOMS");

    public TShirtsFileConverter(File excelFile, File targetDir) {
        super(excelFile, targetDir);
    }

    @Override
    void convert() throws IOException {
        LOGGER.info("Start parsing file {}", inputFile.getAbsolutePath());

        Map<String, Map<String, List<String>>> tShirts = new HashMap<>();
        int nbTShirts = 0;

        try (var workbook = WorkbookFactory.create(inputFile)) {
            Sheet sheet = workbook.getSheetAt(0);

            LOGGER.info("Parsing t-shirt lines..");
            for (var i = 2; i < sheet.getPhysicalNumberOfRows(); i++) {
                org.apache.poi.ss.usermodel.Row row = sheet.getRow(i);
                Cell cellSize = row.getCell(0);
                Cell cellColor = row.getCell(1);
                Cell cellName = row.getCell(2);

                if (cellSize != null && cellColor != null && cellName != null) {
                    try {
                        String size = cellSize.getStringCellValue();
                        String color = cellColor.getStringCellValue();
                        String name = cellName.getStringCellValue().toUpperCase(Locale.ROOT);

                        if (StringUtils.isNoneBlank(size, color, name)) {
                            tShirts.putIfAbsent(size, new HashMap<>());
                            Map<String, List<String>> colorsMap = tShirts.get(size);
                            colorsMap.putIfAbsent(color, new ArrayList<>());
                            List<String> names = colorsMap.get(color);
                            names.add(name);
                            nbTShirts++;
                            LOGGER.debug("Discover new t-shirt {}-{}-{}", size, color, name);
                        }
                    } catch (IllegalStateException e) {
                        // nothing to do
                    }
                }
            }
        }

        tShirts = tShirts
            .entrySet()
            .stream().sorted((e1, e2) -> {
                int comp;
                String e1Key = e1.getKey();
                String e2Key = e2.getKey();
                if (e1Key.contains("/") && e2Key.contains("/")) {
                    comp = Integer.compare(Integer.parseInt(e1Key.substring(0, e1Key.indexOf("/"))),
                        Integer.parseInt(e2Key.substring(0, e2Key.indexOf("/"))));
                } else {
                    comp = e1Key.compareTo(e2Key);
                }

                return comp;
            })
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        createPDF(getOutpuFilePath(".pdf"), tShirts, nbTShirts);
    }

    private void createPDF(Path pdfPath, Map<String, Map<String, List<String>>> tShirts, int nbTShirts)
        throws IOException {
        LOGGER.info("Creating the PDF file...");
        try (PDDocument document = new PDDocument(); OutputStream pdfOS = Files.newOutputStream(pdfPath)) {
            PDPage page = new PDPage(PDRectangle.A4);
            document.addPage(page);

            PDPageContentStream contentStream = new PDPageContentStream(document, page);

            TableBuilder tableBuilder = Table.builder()
                .addColumnsOfWidth(100, 100, 300)
                .padding(2)
                .borderColor(Color.WHITE);

            printTitle(page, contentStream, nbTShirts);
            buildHeader(tableBuilder);
            buildData(tShirts, tableBuilder);

            RepeatedHeaderTableDrawer tableDrawer = RepeatedHeaderTableDrawer.builder()
                .contentStream(contentStream)
                .table(tableBuilder.build())
                .startX(20f)
                .startY(page.getMediaBox().getUpperRightY() - 50f)
                .endY(20f)
                .build();

            tableDrawer.draw(() -> document, () -> new PDPage(PDRectangle.A4), 50f);

            contentStream.close();

            document.save(pdfOS);
        }
    }

    /**
     * Prints the title section of the PDF document.
     * @param page The PDPage object representing a page in the PDF.
     * @param contentStream The PDPageContentStream for adding content to the page.
     * @param nbTShirts The t-shirts number
     * @throws IOException If an I/O error occurs during text rendering.
     */
    private void printTitle(PDPage page, PDPageContentStream contentStream, int nbTShirts) throws IOException {
        contentStream.beginText();
        contentStream.setFont(BOLD_FONT, TITLE_FONT_SIZE);
        contentStream.newLineAtOffset(20, page.getMediaBox().getUpperRightY() - 25);
        contentStream.showText("Commande T-Shirts APEL Saint-Maurille " + LocalDate.now().getYear());
        contentStream.endText();

        contentStream.beginText();
        contentStream.newLineAtOffset(20, page.getMediaBox().getUpperRightY() - 35);
        contentStream.setFont(BOLD_FONT, 9);
        contentStream.showText("Nombre de T-Shirts commandés : " + nbTShirts);
        contentStream.endText();
    }

    /**
     * Builds the header row of the table in the PDF document.
     * @param tableBuilder The TableBuilder used to construct the table structure.
     */
    private void buildHeader(TableBuilder tableBuilder) {
        RowBuilder headerBuilder = Row
            .builder()
            .horizontalAlignment(HorizontalAlignment.CENTER)
            .verticalAlignment(VerticalAlignment.MIDDLE)
            .borderStyle(BorderStyle.SOLID)
            .borderWidth(1)
            .backgroundColor(HEADER_COLOR)
            .textColor(Color.WHITE)
            .font(BOLD_FONT)
            .fontSize(9)
            .height(25f);

        HEADERS.forEach(header -> headerBuilder
            .add(TextCell
                .builder()
                .text(header)
                .build()
            )
        );

        tableBuilder.addRow(headerBuilder.build());
    }

    /**
     * Builds the data rows of the table in the PDF document.
     * @param tShirts Map containing t-shirts.
     * @param tableBuilder The TableBuilder used to construct the table structure.
     */
    private void buildData(Map<String, Map<String, List<String>>> tShirts, TableBuilder tableBuilder) {
        boolean toogle = false;
        for (Entry<String, Map<String, List<String>>> tShirt : tShirts.entrySet()) {
            String size = tShirt.getKey();
            Map<String, List<String>> colors = tShirt.getValue();

            colors = colors
                .entrySet()
                .stream().sorted(Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

            int totalNames = colors.values().stream()
                .mapToInt(List::size)
                .sum();
            int sizeRowNum = 0;
            for (Entry<String, List<String>> color : colors.entrySet()) {
                String colorName = color.getKey();
                List<String> names = color.getValue();
                Collections.sort(names);

                int colorRowNum = 0;
                for (String name : names) {
                    RowBuilder dataBuilder = Row
                        .builder()
                        .borderWidth(1)
                        .font(BOLD_FONT)
                        .padding(5)
                        .textColor(Color.BLACK)
                        .backgroundColor(toogle ? EVEN_ROW_BG_COLOR : ODD_ROW_BG_COLOR);

                    if (sizeRowNum == 0) {
                        dataBuilder.add(TextCell
                            .builder()
                            .rowSpan(totalNames)
                            .text(size)
                            .fontSize(12)
                            .horizontalAlignment(HorizontalAlignment.CENTER)
                            .verticalAlignment(VerticalAlignment.MIDDLE)
                            .build()
                        );
                        sizeRowNum++;
                    }
                    if (colorRowNum == 0) {
                        dataBuilder
                            .add(TextCell
                                .builder()
                                .rowSpan(names.size())
                                .text(colorName)
                                .fontSize(9)
                                .horizontalAlignment(HorizontalAlignment.CENTER)
                                .verticalAlignment(VerticalAlignment.MIDDLE)
                                .build()
                            );
                        colorRowNum++;
                    }

                    dataBuilder
                        .add(TextCell
                            .builder()
                            .fontSize(9)
                            .text(name)
                            .build()
                        );
                    tableBuilder.addRow(dataBuilder.build());
                }
            }
            toogle = !toogle;
        }
    }
}
